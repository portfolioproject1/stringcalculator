﻿namespace StringCalculator
{
    interface ICheckIError
    {
        // Проверка соответствия открывающихся и закрывающихся скобок: ((1-5) 
        // и в правильной ли последовательности отрываются и закрываются
        bool IsValidOpenCloseBracket(MyList<string> expressionArray);

        // Проверка на лишние операторы выражения и на идущие подряд: +*
        bool IsValidOperation(MyList<string> expressionArray);

        // Проверка экранирования отрицательного значения скобками: 1+(-1)
        bool IsValidNegativeOperator(MyList<string> expressionArray);

        // Проверка разделения чисел операторами: 8 9 *5
        bool IsValidCorrectSeparation(MyList<string> expressionArray);

        //Проверка на наличие букв
        bool IsNotLetters(MyList<string> expressionArray);
    }
}
