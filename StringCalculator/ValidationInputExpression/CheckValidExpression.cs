﻿using System.Collections.Generic;

namespace StringCalculator
{
    /// <summary>
    /// Класс проверки корректности введеного выражения 
    /// </summary>
    class CheckValidExpression : ICheckIError
    {
        /// <summary>
        /// Проверка на наличие букв в выражении
        /// </summary>
        /// <param name="expression">Входное выражение</param>
        /// <returns>true-если нет ошибок, false- если ошибки есть</returns>
        public bool IsNotLetters(MyList<string> expressionArray)
        {
            bool result = true;
            foreach (var item in expressionArray)
            {
                if (item != null)
                {
                    double number;
                    if (!double.TryParse(item, out number))
                    {
                        if (char.IsLetter(char.Parse(item)))
                            result = false;
                    }
                }
            }
            return result;
            //return !expressionArray.Any(n => char.IsLetter(char.Parse(n)));
        }

        /// <summary>
        /// Проверка соответствия открывающихся и закрывающихся скобок: ((1-5)  
        /// и в правильной ли последовательности отрываются и закрываются
        /// </summary>
        /// <param name="expression">Входное выражение</param>
        /// <returns>true-если нет ошибок, false- если ошибки есть</returns>
        public bool IsValidCorrectSeparation(MyList<string> expressionArray)
        {
            Stack<string> openBracket = new Stack<string>();
            foreach (var item in expressionArray)
            {
                if (item == "(")
                    openBracket.Push(item);
                if (item == ")")
                {
                    if (openBracket.Count == 0)
                        return false;
                    else
                        openBracket.Pop();
                }
            }
            return openBracket.Count == 0 ? true : false;
        }

        /// <summary>
        /// Проверка на лишние операторы выражения (не указанные в задании)
        /// </summary>
        /// <param name="expression">Входное выражение</param>
        /// <returns>true-если нет ошибок, false- если ошибки есть</returns>
        public bool IsValidNegativeOperator(MyList<string> expressionArray)
        {
            foreach (var item in expressionArray)
            {
                if (item != " " && !string.IsNullOrEmpty(item))
                {
                    double number;
                    if (!double.TryParse(item, out number))
                    {
                        //проверяем что символ может быть только + - * / 
                        if (item != "+" && item != "-" &&
                            item != "/" && item != "*" &&
                            item != "(" && item != ")")
                            return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Проверка экранирования отрицательного значения скобками: 1+(-1)
        /// </summary>
        /// <param name="expression">Входное выражение</param>
        /// <returns>true-если нет ошибок, false- если ошибки есть</returns>
        public bool IsValidOpenCloseBracket(MyList<string> expressionArray)
        {
            for (int i = 0; i < expressionArray.Count; i++)
            {
                if (expressionArray[i] != " " && !string.IsNullOrEmpty(expressionArray[i]))
                {
                    double number;
                    if (!double.TryParse(expressionArray[i], out number))
                    {
                        if (i + 1 != expressionArray.Count)
                        {
                            //Проверяем, что подрят не идут операторы выражения
                            if (expressionArray[i] == "+" || expressionArray[i] == "-" ||
                                expressionArray[i] == "/" || expressionArray[i] == "*")
                            {
                                if (expressionArray[i + 1] == "+" || expressionArray[i + 1] == "-" ||
                               expressionArray[i + 1] == "/" || expressionArray[i + 1] == "*")
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Проверка разделения чисел операторами: 8 9 *5
        /// проверяется отношение один оператор на два значения
        /// </summary>
        /// <param name="expression">Входное выражение</param>
        /// <returns>true-если нет ошибок, false- если ошибки есть</returns>
        public bool IsValidOperation(MyList<string> expressionArray)
        {
            int countOperators = 0;
            int countValues = 0;
            foreach (var item in expressionArray)
            {
                if (item != " " && !string.IsNullOrEmpty(item))
                {
                    double number;
                    if (double.TryParse(item, out number))
                        countValues++;
                    else
                    {
                        if (item != "(" && item != ")" && item != "." && item != ",")
                            countOperators++;
                    }
                }
            }
            return countValues - 1 == countOperators ? true : false;
        }
    }
}
