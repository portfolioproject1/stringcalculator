﻿namespace StringCalculator
{
    class Calculate : IOperations
    {
        public double GetResult(char operators, double a, double b)
        {
            double result=0;
            switch (operators)
            {
                case '+':
                    result = Addition(a, b);
                    break;
                case '-':
                    result = Subtraction(a, b);
                    break;
                case '/':
                    result = Division(a, b);
                    break;
                case '*':
                    result = Multiplication(a, b);
                    break;
            }
            return result;
        }
        public double Addition(double a, double b)
        {
            return a + b;
        }

        public double Division(double a, double b)
        {
            return a / b;
        }

        public double Multiplication(double a, double b)
        {
            return a * b;
        }

        public double Subtraction(double a, double b)
        {
            return a - b;
        }
    }
}
