﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите выражение:");
            string inputExpression = Console.ReadLine();
            //Разбиваем выражение на операнды
            MyList<string> myList = DecompositionMember(inputExpression);
            string error;
            //Проверяем ошибки в выражении
            if (IsValidExpression(myList, out error))
                Console.WriteLine($"Результат: {GetResultExpression(myList)}");
            else
                Console.WriteLine(error);
            Console.ReadKey();
        }

        /// <summary>
        /// Проверка на правильность входящей строки
        /// </summary>
        /// <param name="expressionArray">Массив, разделенный на операнды</param>
        /// <param name="error">Сообщение об ошибке</param>
        /// <returns></returns>
        public static bool IsValidExpression(MyList<string> expressionArray, out string error)
        {
            bool status = true;
            error = "";
            string message = "Сообщение об ошибке:";
            CheckValidExpression checkValidExpression = new CheckValidExpression();
            if (!checkValidExpression.IsValidCorrectSeparation(expressionArray))
            {
                error = $"{message} Выражение не прошло проверку на последовательность откр. и закрывающихся скобок";
                status = false;
            }
            if (!checkValidExpression.IsValidNegativeOperator(expressionArray))
            {
                error = $"{message} Выражение не прошло проверку, т к в отличии от указанных(+,-,*,/) используются иные операторы ";
                status = false;
            }
            if (!checkValidExpression.IsValidOpenCloseBracket(expressionArray))
            {
                error = $"{message} Выражение не прошло проверку на последовательность откр. и закрывающихся скобок";
                status = false;
            }
            if (!checkValidExpression.IsValidOperation(expressionArray))
            {
                error = $"{message} Выражение не прошло проверку, т к нарушена последовательность операндов";
                status = false;
            }
            if (!checkValidExpression.IsNotLetters(expressionArray))
            {
                error = $"{message} Выражение не прошло проверку, т к выражение не должно содержать букв";
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Разложение выражения на члены и операторы
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <returns></returns>
        public static MyList<string> DecompositionMember(string expression)
        {
            MyList<string> myList = new MyList<string>();
            string number = "";
            //Перебор строки выражения
            for (int i = 0; i < expression.Length; i++)
            {
                // если элемент массива символ и он не ',' или '.' то мы считаем, что 
                // это оператор или открывающаяся или закрывающаяся скобка
                // иначе это число, которое с помощью конкатенации добавляем в переменную number
                // и в результате получим число с плавающей точкой или без
                if (!char.IsDigit(expression, i))
                {
                    if (expression[i] != ',' && expression[i] != '.')
                    {
                        if (!string.IsNullOrEmpty(number))
                        {
                            myList.Add(number);
                            number = "";
                        }
                        myList.Add(expression[i].ToString());
                    }
                    else
                        number += expression[i];
                }
                else
                {
                    if (expression[i] == ' ')
                        myList.Add(expression[i].ToString());
                    else
                    {
                        number += expression[i];
                        if (i + 1 == expression.Length)
                            myList.Add(number);
                    }
                }
            }
            return myList;
        }

        public static double GetResultExpression(MyList<string> expression)
        {
            //создаем 2-стека, для чисел и для операторов
            Stack<double> numbers = new Stack<double>();
            Stack<char> operators = new Stack<char>();

            Calculate calculate = new Calculate();

            foreach (var item in expression)
            {
                double number;
                //если число, то добавляем в стек
                if (double.TryParse(item, out number))
                    numbers.Push(number);
                else
                {
                    //проверка на пустоту и пробелы
                    if (item != " " && !string.IsNullOrEmpty(item))
                    {
                        //получаем приоритет текужего оператора выражения
                        int preority = GetPriority(item);
                        int lastStatusOperator = 0;
                        if (operators.Count == 0)
                        {
                            operators.Push(char.Parse(item));
                            continue;
                        }
                        else
                            // получаем приоритет последнего оператора с стеке
                            lastStatusOperator = GetPriority(operators.Peek().ToString());
                        if (preority == 0)
                        {
                            if (item == "(")
                            {
                                operators.Push(char.Parse(item));
                                continue;
                            }
                            else
                            {
                                int counterOp = 0;
                                foreach (var op in operators)
                                {
                                    if (op != '(')
                                    {
                                        double num1 = numbers.Pop();
                                        double num2 = numbers.Pop();
                                        numbers.Push(calculate.GetResult(op, num2, num1));
                                        counterOp++;
                                    }
                                    else
                                    {
                                        operators.Pop();
                                        break;
                                    }
                                }
                                for (int i = 0; i < counterOp; i++)
                                    operators.Pop();
                            }
                            continue;
                        }

                        //если приоритет последнего оператора в стеке меньше текущего,
                        //то добавляем его в стек
                        if (lastStatusOperator < preority)
                        {
                            operators.Push(char.Parse(item));
                            continue;
                        }
                        bool flag = true;
                        while (flag)
                        {
                            //если приоритет последнего оператора в стеке равен текущему или больше его ,
                            //берем из стека чисел последние два числа и выполняем последнюю операцию из стека
                            if (lastStatusOperator == preority || lastStatusOperator > preority)
                            {
                                if (numbers.Count != 1)
                                {
                                    double num1 = numbers.Pop();
                                    double num2 = numbers.Pop();
                                    numbers.Push(calculate.GetResult(operators.Pop(), num2, num1));
                                    if (operators.Count != 0)
                                        lastStatusOperator = GetPriority(operators.Peek().ToString());
                                }
                                else
                                {
                                    operators.Push(char.Parse(item));
                                    flag = false;
                                }
                            }

                            else
                            {
                                operators.Push(char.Parse(item));
                                flag = false;
                            }
                        }
                    }
                }
            }
            if (operators.Count == 1 && numbers.Count == 2)
            {
                double num1 = numbers.Pop();
                double num2 = numbers.Pop();
                numbers.Push(calculate.GetResult(operators.Pop(), num2, num1));
            }

            return numbers.Peek();
        }

        /// <summary>
        ///  Таблица преоритетов
        /// </summary>
        /// <param name="symbole">Оператор</param>
        /// <returns>Преоритет оператора</returns>
        public static int GetPriority(string inputOperator)
        {
            int status = 0;
            switch (inputOperator)
            {
                case "(":
                    status = 0;
                    break;
                case ")":
                    status = 0;
                    break;
                case "+":
                    status = 1;
                    break;
                case "-":
                    status = 1;
                    break;
                case "*":
                    status = 2;
                    break;
                case "/":
                    status = 2;
                    break;
            }
            return status;
        }
    }
}
