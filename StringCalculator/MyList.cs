﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace StringCalculator
{
    public class MyList<T> : IList<T>
    {

        private T[] myElements = new T[8];
        private int _count;

        public MyList()
        {
            _count = 0;
        }
        public MyList(T[] elements)
        {
            foreach (var item in elements)
            {
                Add(item);
            }
        }
        public T this[int index] { get { return myElements[index]; } set { myElements[index] = value; } }

        public int Count { get { return _count; } }

        public bool IsReadOnly { get { return false; } }

        public void Add(T item)
        {
            if (_count < myElements.Length)
            {
                myElements[_count] = item;
                _count++;
            }
            else
            {
                T[] newListArray = new T[myElements.Length * 2];
                Array.Copy(myElements, newListArray, myElements.Length);
                myElements = newListArray;
                myElements[_count] = item;
                _count++;
            }
        }

        public void Clear()
        {
            _count = 0;
            myElements = new T[8];
        }

        public bool Contains(T item)
        {
            bool isExistItem = false;
            for (int i = 0; i < Count; i++)
            {
                if ((object)myElements[i] == (object)item)
                {
                    isExistItem = true;
                    break;
                }
            }
            return isExistItem;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            int j = arrayIndex;
            for (int i = 0; i < Count; i++)
            {
                array.SetValue(myElements[i], j);
                j++;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < myElements.Length; i++)
                yield return myElements[i];
        }

        public int IndexOf(T item)
        {
            int indexItem = -1;
            for (int i = 0; i < Count; i++)
            {
                if ((object)myElements[i] == (object)item)
                {
                    indexItem = i;
                    break;
                }
            }
            return indexItem;
        }

        public void Insert(int index, T item)
        {
            if ((_count + 1 <= myElements.Length) && (index < Count) && (index >= 0))
            {
                _count++;
                for (int i = Count - 1; i > index; i--)
                {
                    myElements[i] = myElements[i - 1];
                }
                myElements[index] = item;
            }
        }


        public bool Remove(T item)
        {
            int indexElement = IndexOf(item);
            if ((indexElement >= 0) && (indexElement < Count))
            {
                for (int i = indexElement; i < Count - 1; i++)
                {
                    myElements[i] = myElements[i + 1];
                }
                _count--;
                return true;
            }
            return false;

        }

        public void RemoveAt(int index)
        {
            if ((index >= 0) && (index < Count))
            {
                for (int i = index; i < Count - 1; i++)
                {
                    myElements[i] = myElements[i + 1];
                }
                _count--;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return myElements.GetEnumerator();
        }


        //public bool MoveNext()
        //{
        //    throw new NotImplementedException();
        //}
        //public void Reset()
        //{
        //    throw new NotImplementedException();
        //}
        //public void Dispose()
        //{
        //    throw new NotImplementedException();
        //}

        //private T _current;
        //public T Current
        //{
        //    get
        //    {
        //        if (_current == null)
        //        {
        //            throw new InvalidOperationException();
        //        }
        //        return _current;
        //    }
        //}

        //object IEnumerator.Current { get { return Current1; } }

    }
}
