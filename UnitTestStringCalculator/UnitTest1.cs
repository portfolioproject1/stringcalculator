﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringCalculator;
using System.Data;

namespace UnitTestStringCalculator
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestEqualExpresion()
        {
            string testStringExpression = @"((( 10.3 - 9.3 ) +2.3)* ( 9 + 1 ))/9";
            MyList<string> myList = Program.DecompositionMember(testStringExpression);
            double result1 = Program.GetResultExpression(myList);
            double result2 = double.Parse(new DataTable().Compute(testStringExpression, null).ToString());
            Assert.AreEqual(result1, result2);
        }
    }
}
